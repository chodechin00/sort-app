package com.andrei.sorting;

import java.util.Arrays;

public class Sorting {

    public void sort(int[] array){

        if(array == null) {
            throw new IllegalArgumentException();
        }
        Arrays.sort(array);
    }

    private int[] parseToIntArray(String[] args) {

        if(!correctLength(args)) {
            throw new IllegalArgumentException("Input takes less than 10 numbers");
        }

        int[] integers = new int[args.length];

        try {
            for (int i = 0; i < args.length; i++) {
                integers[i] = Integer.parseInt(args[i]);
            }
        } catch (Exception e){
            throw new IllegalArgumentException("Insert correct data");
        }
        return integers;
    }

    private boolean correctLength(String[] args) {
        return args.length <= 10;
    }
    public static void main(String[] args) {

        Sorting sorting = new Sorting();
        int[] integers = sorting.parseToIntArray(args);
        sorting.sort(integers);
        System.out.println(Arrays.toString(integers));
    }
}
