package com.andrei.sorting;

import org.junit.Test;
import java.util.Arrays;
import static org.junit.Assert.*;

public class SortingTest {

    Sorting sorting = new Sorting();

    @Test(expected = IllegalArgumentException.class)
    public void testNullCase(){
        sorting.sort(null);
    }

    @Test
    public void testEmptyCase(){

        int[] actual = new int[] {};
        sorting.sort(actual);

        assertEquals(0,actual.length);
    }

    @Test
    public void testSingleElementArrayCase() {

        int[] array = new int[] {1};

        int[] actual = new int[] {1};
        sorting.sort(actual);

        assertEquals(array[0],actual[0]);

    }

    @Test
    public void testSortedArraysCase() {

        int[] notSorted = new int[] {1,2,2,3,3,4,4,5,6,8,9};
        int[] sorted = new int[] {1,2,2,3,3,4,4,5,6,8,9};

        int[] copy = Arrays.copyOf(notSorted, notSorted.length);
        sorting.sort(copy);

        assertArrayEquals(sorted,copy);
    }

    @Test
    public void testOtherCases() {

        int[] notSorted = new int[] {5,4,3,8,1,2,6,9,4,2,3};
        int[] sorted = new int[] {1,2,2,3,3,4,4,5,6,8,9};

        int[] copy = Arrays.copyOf(notSorted, notSorted.length);
        sorting.sort(copy);

        assertArrayEquals(sorted,copy);
    }

}